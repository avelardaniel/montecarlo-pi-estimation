#[allow(warnings)]

use rand::Rng;

fn main() {
    let size = 5_000_000; // N pontos no universo
    let mut inside = 0;
    let mut rng = rand::thread_rng();
    
    for _ in 1..=size{
        let (x, y) = (rng.gen_range(-1.0..1.0) as f32, rng.gen_range(-1.0..1.0) as f32);
        let dist = f32::sqrt(x.powf(2.0)+y.powf(2.0));

        if dist <= 1f32 {
            inside += 1;
        }      
    }
    println!("{}", 4f32*(inside as f32/size as f32));
}
